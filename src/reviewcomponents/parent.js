import { Provider } from "react-redux";
import store from "../stores/counter-store";
import Child from "./child";



export default function Parent() {
    const adam = "Adam";
    const bill = "Bill";
    const steve = "Steve";

    return (
        <div>
            <Provider store={store}>
                <Child fName="Adam"></Child>
                <Child fName={"Bill"}></Child>
                <Child fName={steve}></Child>
            </Provider>



        </div>)
}